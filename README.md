# enby bot dictionary

welcome to the enby bot dictionary. this is the file that enby bot (EnbyBot@botsin.space) gets the sentences and word from.

## formatting

### "(choice) enby"

enby bot normally posts stuff in the following format

```(choice) enby```

`choice` is whatever line that was randomly picked from `dictionary.txt`. so for example if it chose line 1 "lovely", it will post the following

```lovely enby```

### "(choice)"

alternatively, if the line starts with "**" (ignore quotation marks), then it is shown in format

```(choice)```

without the enby. so for example if it chose line 217 "enbies are cool", it will post the following

```enbies are cool```

## ignore this

btw, lines 1 and 217, used in the examples above, are actually present on the `dictionary.txt` file!

## contributing
you can suggest stuff, just contact me at mastodon or open an issue with the changes you want being made, or a pull request with the added words.